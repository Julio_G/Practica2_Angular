angular.module('myApp', [])
        .controller('controlador', ['$scope',
            function ($scope) {
                $scope.opcio1 = 1;
                $scope.opcio2 = 1;
                $scope.opcio3 = 1;
                $scope.crearCombinacion = true;
                $scope.validaCombinacion = false;
                $scope.password = [];

                $scope.clicaOpcio = function () {
                    $scope.opcio1 += 1;
                    if ($scope.opcio1 > 4) {
                        $scope.opcio1 = 1;
                    }
                };
                $scope.clicaOpcio2 = function () {
                    $scope.opcio2 += 1;
                    if ($scope.opcio2 > 4) {
                        $scope.opcio2 = 1;
                    }
                };
                $scope.clicaOpcio3 = function () {
                    $scope.opcio3 += 1;
                    if ($scope.opcio3 > 4) {
                        $scope.opcio3 = 1;
                    }
                };
                $scope.aceptaCombinacion = function () {
                    $scope.crearCombinacion = false;
                    $scope.validaCombinacion = true;
                    $scope.password = [$scope.opcio1, $scope.opcio2, $scope.opcio3];
                    $scope.opcio1 = 1;
                    $scope.opcio2 = 1;
                    $scope.opcio3 = 1;

                };
                $scope.combinaciones = [];
                //$scope.combinaciones[0]= [1,2,3];
                // $scope.combinaciones[1]= [1,1,4];
                $scope.validarCombinacion = function () {
                    var op1check = "diferent", op2check = "diferent", op3check = "diferent";
                    var contador = 0;
                    if ($scope.opcio1 == $scope.password[0]) {
                        op1check = "igual";
                        contador++;
                    }
                    if ($scope.opcio2 == $scope.password[1]) {
                        op2check = "igual";
                        contador++;
                    }
                    if ($scope.opcio3 == $scope.password[2]) {
                        op3check = "igual";
                        contador++;
                    }
                    if(contador == 3){
                        
                    }
                    var combi = [[$scope.opcio1, op1check],
                        [$scope.opcio2, op2check],
                        [$scope.opcio3, op3check]];
                    $scope.combinaciones.push(combi);
                };
            }]);